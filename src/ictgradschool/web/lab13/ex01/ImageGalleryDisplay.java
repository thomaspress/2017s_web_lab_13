package ictgradschool.web.lab13.ex01;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by cku782 on 10/01/2018.
 */
public class ImageGalleryDisplay extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter pr = resp.getWriter();

        List <File> images= new ArrayList<>();

        ServletContext servletContext = getServletContext();

        String fullPhotoPath = servletContext.getRealPath("/Photos");

        File folder = new File(fullPhotoPath);
        File[] listOfFile = folder.listFiles();

        for (File a : listOfFile) {
            if(a.getName().contains("_thumbnail.png")){
                images.add(a);
            }
            //System.out.println(a.length());
        }
        pr.println("<html>\n<head><title>Image Gallery Viewer</title> </head> <body> <table> ");

        for (File a: images){
            int b= a.getName().indexOf("_thumbnail");
            String c= a.getName().substring(0,b);
            pr.print("<tr> <td> <a href=\" /Photos/" +c+ ".jpg\"> <img src=\"/Photos/" +a.getName() + "\"> </a> </td> <td>" + c + "</td> <td>" + a.length() + "</td> </tr>" );

        }
        pr.println("</table> </body> </html>");
    }

}
